package io.github.drymarev.neptune

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.support.v4.util.ArrayMap
import io.github.drymarev.neptune.constructor_standings.ConstructorStandingsViewModel
import io.github.drymarev.neptune.driver_standings.DriverStandingsViewModel
import io.github.drymarev.neptune.schedule.ScheduleViewModel

@Suppress("UNCHECKED_CAST")
class AppViewModelFactory(component: ViewModelComponent) : ViewModelProvider.Factory {

  private val creators: ArrayMap<Class<*>, () -> ViewModel> = ArrayMap()

  init {
    creators.put(ScheduleViewModel::class.java) { component.scheduleViewModel() }
    creators.put(DriverStandingsViewModel::class.java) { component.driverStandingsViewModel() }
    creators.put(ConstructorStandingsViewModel::class.java) {
      component.constructorStandingsViewModel()
    }
  }

  override fun <T : ViewModel> create(modelClass: Class<T>): T {
    val creator = creators[modelClass]
        ?: creators.entries.find { (key, _) -> modelClass.isAssignableFrom(key) }?.value
        ?: throw IllegalArgumentException("Unknown model class $modelClass.")
    return try {
      creator() as T
    } catch (e: Exception) {
      throw RuntimeException(e)
    }
  }
}
