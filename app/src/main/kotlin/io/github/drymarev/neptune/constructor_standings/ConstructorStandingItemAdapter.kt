package io.github.drymarev.neptune.constructor_standings

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.ViewGroup
import io.github.drymarev.neptune.databinding.ConstructorStandingItemBinding
import io.github.drymarev.neptune.util.inflater
import javax.inject.Inject
import kotlin.properties.Delegates

class ConstructorStandingItemAdapter @Inject constructor() : RecyclerView.Adapter<ConstructorStandingItemViewHolder>() {

  var items by Delegates.observable(emptyList<ConstructorStandingItem>()) { _, old, new ->
    if (old != new) {
      notifyDataSetChanged()
    }
  }

  override fun getItemCount(): Int {
    return items.size
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConstructorStandingItemViewHolder {
    return ConstructorStandingItemViewHolder(parent.driverStandingItemBinding)
  }

  override fun onBindViewHolder(holder: ConstructorStandingItemViewHolder, position: Int) {
    holder.binding.bind(items[position])
  }
}

data class ConstructorStandingItemViewHolder(
    val binding: ConstructorStandingItemBinding
) : ViewHolder(binding.root)

private val ViewGroup.driverStandingItemBinding: ConstructorStandingItemBinding
  inline get() {
    return ConstructorStandingItemBinding.inflate(inflater, this, false)
  }

private fun ConstructorStandingItemBinding.bind(item: ConstructorStandingItem) {
  constructorPosition.text = item.positionText
  constructorName.text = item.constructorItem.name
  constructorPoints.text = item.points
}
