package io.github.drymarev.neptune.constructor_standings

import dagger.Binds
import dagger.Module
import dagger.Provides
import io.github.drymarev.neptune.core.Presenter
import io.github.drymarev.neptune.core.Result
import io.github.drymarev.neptune.core.UseCase

@Module(includes = arrayOf(RemoteLoadConstructorStandingsUseCaseModule::class))
interface ConstructorStandingsPresenterModule {

  @Binds fun bind(presenter: ConstructorStandingsPresenter):
      Presenter<@JvmSuppressWildcards ConstructorStandingsView>
}

@Module class RemoteLoadConstructorStandingsUseCaseModule {

  @Provides fun provide(useCase: RemoteLoadConstructorStandingsUseCase):
      UseCase<LoadConstructorStandingsAction, Result<ConstructorStandingRecords>> {
    return useCase
  }
}
