package io.github.drymarev.neptune.constructor_standings

import android.arch.lifecycle.ViewModel
import com.github.ajalt.timberkt.i
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import io.github.drymarev.neptune.core.Presenter
import io.reactivex.Observable
import io.reactivex.functions.Consumer
import javax.inject.Inject

class ConstructorStandingsViewModel @Inject constructor(
    val presenter: Presenter<@JvmSuppressWildcards ConstructorStandingsView>
) : ViewModel(), ConstructorStandingsView {

  private val loadDriverStandingsIntentRelay = PublishRelay.create<LoadConstructorStandingsIntent>()
  private val modelRelay = BehaviorRelay.create<ConstructorStandingsModel>()

  val loadConstructorStandingsConsumer: Consumer<LoadConstructorStandingsIntent>
    get() {
      return loadDriverStandingsIntentRelay
    }
  val models: Observable<ConstructorStandingsModel>
    get() {
      return modelRelay.hide()
    }

  init {
    presenter.attach(this)
  }

  override fun onCleared() {
    presenter.detach(this)
  }

  override fun loadConstructorStandingsIntent(): Observable<LoadConstructorStandingsIntent> {
    return loadDriverStandingsIntentRelay.hide().distinctUntilChanged()
  }

  override fun render(model: ConstructorStandingsModel) {
    i { model.toString() }
    modelRelay.accept(model)
  }
}
