package io.github.drymarev.neptune.driver_standings

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.ViewGroup
import io.github.drymarev.neptune.databinding.DriverStandingItemBinding
import io.github.drymarev.neptune.util.inflater
import javax.inject.Inject
import kotlin.properties.Delegates

class DriverStandingItemAdapter @Inject constructor() : RecyclerView.Adapter<DriverStandingItemViewHolder>() {

  var items by Delegates.observable(emptyList<DriverStandingItem>()) { _, old, new ->
    if (old != new) {
      notifyDataSetChanged()
    }
  }

  override fun getItemCount(): Int {
    return items.size
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DriverStandingItemViewHolder {
    return DriverStandingItemViewHolder(parent.driverStandingItemBinding)
  }

  override fun onBindViewHolder(holder: DriverStandingItemViewHolder, position: Int) {
    holder.binding.bind(items[position])
  }
}

data class DriverStandingItemViewHolder(
    val binding: DriverStandingItemBinding
) : ViewHolder(binding.root)

private val ViewGroup.driverStandingItemBinding: DriverStandingItemBinding
  inline get() {
    return DriverStandingItemBinding.inflate(inflater, this, false)
  }

private fun DriverStandingItemBinding.bind(item: DriverStandingItem) {
  driverPosition.text = item.positionText
  driverName.text = "${item.driverItem.givenName} ${item.driverItem.familyName}"
  driverConstructors.text = item.constructorItems.map { it.name }.joinToString()
  driverPoints.text = item.points
}
