package io.github.drymarev.neptune.driver_standings

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.trello.rxlifecycle2.components.support.RxFragment
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjection
import io.github.drymarev.neptune.PerFragment
import io.github.drymarev.neptune.databinding.DriverStandingsFragmentBinding
import io.github.drymarev.neptune.util.get
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject
import kotlin.properties.Delegates

class DriverStandingsFragment : RxFragment() {

  @Inject lateinit var factory: ViewModelProvider.Factory
  @Inject lateinit var adapter: DriverStandingItemAdapter

  private var binding by Delegates.notNull<DriverStandingsFragmentBinding>()
  private var model by Delegates.notNull<DriverStandingsViewModel>()

  override fun onAttach(context: Context?) {
    AndroidSupportInjection.inject(this)
    super.onAttach(context)
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)
    model = ViewModelProviders.of(this, factory).get()
  }

  override fun onCreateView(
      inflater: LayoutInflater,
      container: ViewGroup?,
      savedInstanceState: Bundle?
  ): View? {
    binding = DriverStandingsFragmentBinding.inflate(inflater, container, false).also {
      it.list.layoutManager = LinearLayoutManager(activity)
      it.list.adapter = adapter
    }
    return binding.root
  }

  override fun onDestroyView() {
    binding.list.adapter = null
    super.onDestroyView()
  }

  override fun onStart() {
    super.onStart()
    Observable.just(LoadDriverStandingsIntent.CurrentSeason)
        .bindToLifecycle(this)
        .subscribe(model.loadDriverStandingsConsumer)
    model.models
        .bindToLifecycle(this)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeBy(onNext = { adapter.items = it.driverStandings })
  }

  companion object {

    fun newInstance(): Fragment {
      return DriverStandingsFragment()
    }
  }
}

@Module interface DriverStandingsFragmentModule {

  @PerFragment @ContributesAndroidInjector fun fragment(): DriverStandingsFragment
}
