package io.github.drymarev.neptune.driver_standings

import dagger.Binds
import dagger.Module
import dagger.Provides
import io.github.drymarev.neptune.core.Presenter
import io.github.drymarev.neptune.core.Result
import io.github.drymarev.neptune.core.UseCase

@Module(includes = arrayOf(RemoteLoadDriverStandingsUseCaseModule::class))
interface DriverStandingsPresenterModule {

  @Binds fun bind(presenter: DriverStandingsPresenter):
      Presenter<@JvmSuppressWildcards DriverStandingsView>
}

@Module class RemoteLoadDriverStandingsUseCaseModule {

  @Provides fun provide(useCase: RemoteLoadDriverStandingsUseCase):
      UseCase<LoadDriverStandingsAction, Result<DriverStandingRecords>> {
    return useCase
  }
}
