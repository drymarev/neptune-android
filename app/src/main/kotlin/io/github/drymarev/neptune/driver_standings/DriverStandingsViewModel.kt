package io.github.drymarev.neptune.driver_standings

import android.arch.lifecycle.ViewModel
import com.github.ajalt.timberkt.i
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import io.github.drymarev.neptune.core.Presenter
import io.reactivex.Observable
import io.reactivex.functions.Consumer
import javax.inject.Inject

class DriverStandingsViewModel @Inject constructor(
    val presenter: Presenter<@JvmSuppressWildcards DriverStandingsView>
) : ViewModel(), DriverStandingsView {

  private val loadDriverStandingsIntentRelay = PublishRelay.create<LoadDriverStandingsIntent>()
  private val modelRelay = BehaviorRelay.create<DriverStandingsModel>()

  val loadDriverStandingsConsumer: Consumer<LoadDriverStandingsIntent>
    get() {
      return loadDriverStandingsIntentRelay
    }
  val models: Observable<DriverStandingsModel>
    get() {
      return modelRelay.hide()
    }

  init {
    presenter.attach(this)
  }

  override fun onCleared() {
    presenter.detach(this)
  }

  override fun loadDriverStandingsIntent(): Observable<LoadDriverStandingsIntent> {
    return loadDriverStandingsIntentRelay.hide().distinctUntilChanged()
  }

  override fun render(model: DriverStandingsModel) {
    i { model.toString() }
    modelRelay.accept(model)
  }
}
