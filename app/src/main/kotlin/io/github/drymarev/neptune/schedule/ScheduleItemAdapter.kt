package io.github.drymarev.neptune.schedule

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.ViewGroup
import io.github.drymarev.neptune.databinding.ScheduleItemBinding
import io.github.drymarev.neptune.util.inflater
import org.threeten.bp.format.DateTimeFormatter
import javax.inject.Inject
import kotlin.properties.Delegates

class ScheduleItemAdapter @Inject constructor() : RecyclerView.Adapter<ScheduleItemViewHolder>() {

  var items by Delegates.observable(emptyList<ScheduleItem>()) { _, old, new ->
    if (old != new) {
      notifyDataSetChanged()
    }
  }

  override fun getItemCount(): Int {
    return items.size
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScheduleItemViewHolder {
    return ScheduleItemViewHolder(parent.scheduleItemBinding)
  }

  override fun onBindViewHolder(holder: ScheduleItemViewHolder, position: Int) {
    holder.binding.provide(items[position])
  }
}

data class ScheduleItemViewHolder(val binding: ScheduleItemBinding) : ViewHolder(binding.root)

private val ViewGroup.scheduleItemBinding: ScheduleItemBinding
  inline get() {
    return ScheduleItemBinding.inflate(inflater, this, false)
  }

private fun ScheduleItemBinding.provide(item: ScheduleItem) {
  raceName.text = item.raceName
  raceLocation.text = "${item.circuitLocality}, ${item.circuitCountry}"
  raceMonth.text = MONTH_FORMATTER.format(item.time)
  raceDay.text = DAY_FORMATTER.format(item.time)
}

private val MONTH_FORMATTER = DateTimeFormatter.ofPattern("MMM")
private val DAY_FORMATTER = DateTimeFormatter.ofPattern("d")
