package io.github.drymarev.neptune.util

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

inline fun <reified T : ViewModel> ViewModelProvider.get(): T {
  return this[T::class.java]
}
