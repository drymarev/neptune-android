package io.github.drymarev.neptune.constructor_standings

sealed class LoadConstructorStandingsAction {
  object CurrentSeason : LoadConstructorStandingsAction()
  data class Season(val season: String) : LoadConstructorStandingsAction()
}

data class ConstructorStandingRecord(
    val season: String,
    val round: String,
    val position: String,
    val positionText: String,
    val points: String,
    val wins: String,
    val constructorRecord: ConstructorRecord
)

data class ConstructorRecord(
    val id: String,
    val url: String,
    val name: String,
    val nationality: String
)

typealias ConstructorStandingRecords = List<ConstructorStandingRecord>
