package io.github.drymarev.neptune.driver_standings

import io.github.drymarev.neptune.constructor_standings.ConstructorRecord
import org.threeten.bp.LocalDate

sealed class LoadDriverStandingsAction {
  object CurrentSeason : LoadDriverStandingsAction()
  data class Season(val season: String) : LoadDriverStandingsAction()
}

data class DriverStandingRecord(
    val season: String,
    val round: String,
    val position: String,
    val positionText: String,
    val points: String,
    val wins: String,
    val driverRecord: DriverRecord,
    val constructorRecords: List<ConstructorRecord>
)

data class DriverRecord(
    val id: String,
    val permanentNumber: String,
    val code: String,
    val url: String,
    val givenName: String,
    val familyName: String,
    val dateOfBirth: LocalDate,
    val nationality: String
)

typealias DriverStandingRecords = List<DriverStandingRecord>
