package io.github.drymarev.neptune.core

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign

interface View

interface Presenter<in T : View> {

  fun attach(view: T)

  fun detach(view: T)
}

typealias Reducer<Model, PartialModel> = (Model, PartialModel) -> Model

abstract class RxPresenter<in T : View> : Presenter<T> {

  private val disposables = CompositeDisposable()

  protected fun add(disposable: Disposable) {
    disposables += disposable
  }

  override final fun detach(view: T) {
    disposables.clear()
  }
}
