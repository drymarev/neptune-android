package io.github.drymarev.neptune.driver_standings

import io.github.drymarev.neptune.constructor_standings.ConstructorItem
import io.github.drymarev.neptune.constructor_standings.constructorItem
import io.github.drymarev.neptune.core.Reducer
import io.github.drymarev.neptune.core.Result
import io.github.drymarev.neptune.core.RxPresenter
import io.github.drymarev.neptune.core.UseCase
import io.github.drymarev.neptune.core.View
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.threeten.bp.LocalDate
import javax.inject.Inject

interface DriverStandingsView : View {

  fun loadDriverStandingsIntent(): Observable<LoadDriverStandingsIntent>

  fun render(model: DriverStandingsModel)
}

sealed class LoadDriverStandingsIntent {
  object CurrentSeason : LoadDriverStandingsIntent()
  data class Season(val season: String) : LoadDriverStandingsIntent()
}

data class DriverItem(
    val id: String,
    val permanentNumber: String,
    val code: String,
    val url: String,
    val givenName: String,
    val familyName: String,
    val dateOfBirth: LocalDate,
    val nationality: String
)

data class DriverStandingItem(
    val season: String,
    val round: String,
    val position: String,
    val positionText: String,
    val points: String,
    val wins: String,
    val driverItem: DriverItem,
    val constructorItems: List<ConstructorItem>
)

data class DriverStandingsModel(
    val loadingDriverStandings: Boolean = false,
    val driverStandings: List<DriverStandingItem> = emptyList(),
    val driverStandingsError: Throwable? = null
)

sealed class DriverStandingsPartialModel {
  object LoadingDriverStandings : DriverStandingsPartialModel()
  data class DriverStandingsLoaded(
      val items: List<DriverStandingItem>
  ) : DriverStandingsPartialModel()

  data class DriverStandingsError(val error: Throwable) : DriverStandingsPartialModel()
}

internal object DriverStandingsReducer : Reducer<DriverStandingsModel, DriverStandingsPartialModel> {
  override fun invoke(model: DriverStandingsModel,
      changes: DriverStandingsPartialModel): DriverStandingsModel {
    return when (changes) {
      DriverStandingsPartialModel.LoadingDriverStandings -> model.copy(
          loadingDriverStandings = true,
          driverStandings = emptyList(),
          driverStandingsError = null
      )
      is DriverStandingsPartialModel.DriverStandingsLoaded -> model.copy(
          loadingDriverStandings = false,
          driverStandings = changes.items,
          driverStandingsError = null
      )
      is DriverStandingsPartialModel.DriverStandingsError -> model.copy(
          loadingDriverStandings = false,
          driverStandings = emptyList(),
          driverStandingsError = changes.error
      )
    }
  }
}

class DriverStandingsPresenter @Inject constructor(
    val loadDriverStandingsUseCase: UseCase<LoadDriverStandingsAction, Result<DriverStandingRecords>>
) : RxPresenter<DriverStandingsView>() {

  override fun attach(view: DriverStandingsView) {
    val loadDriverStandings = view.loadDriverStandingsIntent()
        .observeOn(Schedulers.computation())
        .map {
          when (it) {
            LoadDriverStandingsIntent.CurrentSeason -> LoadDriverStandingsAction.CurrentSeason
            is LoadDriverStandingsIntent.Season -> LoadDriverStandingsAction.Season(it.season)
          }
        }
        .compose(loadDriverStandingsUseCase)
        .map {
          when (it) {
            is Result.Success -> DriverStandingsPartialModel
                .DriverStandingsLoaded(it.value.map { it.driverStandingItem })
            is Result.Failure -> DriverStandingsPartialModel
                .DriverStandingsError(it.error)
          }
        }
        .startWith(DriverStandingsPartialModel.LoadingDriverStandings)
    add(loadDriverStandings.observeOn(Schedulers.computation())
        .scan(DriverStandingsModel(), DriverStandingsReducer)
        .distinctUntilChanged()
        .subscribeBy(onNext = { view.render(it) })
    )
  }
}

private val DriverStandingRecord.driverStandingItem: DriverStandingItem
  inline get() {
    return DriverStandingItem(
        season = season,
        round = round,
        position = position,
        positionText = positionText,
        points = points,
        wins = wins,
        driverItem = driverRecord.driverItem,
        constructorItems = constructorRecords.map { it.constructorItem }
    )
  }

private val DriverRecord.driverItem: DriverItem
  inline get() {
    return DriverItem(
        id = id,
        permanentNumber = permanentNumber,
        code = code,
        url = url,
        givenName = givenName,
        familyName = familyName,
        dateOfBirth = dateOfBirth,
        nationality = nationality
    )
  }
