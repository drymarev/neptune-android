package io.github.drymarev.neptune.driver_standings

import com.google.common.truth.Truth.assertThat
import io.github.drymarev.neptune.constructor_standings.ConstructorItem
import io.github.drymarev.neptune.core.Reducer
import io.github.drymarev.neptune.util.randomUUID
import org.junit.Before
import org.junit.Test
import org.threeten.bp.LocalDate
import kotlin.properties.Delegates
import io.github.drymarev.neptune.driver_standings.DriverStandingsModel as Model
import io.github.drymarev.neptune.driver_standings.DriverStandingsPartialModel as PartialModel

class DriverStandingsModelReducerTest {

  private var reducer by Delegates.notNull<Reducer<Model, PartialModel>>()

  @Before fun setUp() {
    reducer = DriverStandingsReducer
  }

  @Test fun loadingDriverStandingsPartialModelProducesCorrectModel() {
    val model = Model(
        loadingDriverStandings = false,
        driverStandings = list,
        driverStandingsError = Throwable()
    )
    val changes = PartialModel.LoadingDriverStandings
    assertThat(reducer(model, changes)).isEqualTo(model.copy(
        loadingDriverStandings = true,
        driverStandings = emptyList(),
        driverStandingsError = null
    ))
  }

  @Test fun driverStandingsErrorPartialModelProducesCorrectModel() {
    val model = Model(
        loadingDriverStandings = true,
        driverStandings = list,
        driverStandingsError = null
    )
    val changes = PartialModel.DriverStandingsError(Throwable())
    assertThat(reducer(model, changes)).isEqualTo(model.copy(
        loadingDriverStandings = false,
        driverStandings = emptyList(),
        driverStandingsError = changes.error
    ))
  }

  @Test fun driverStandingsLoadedPartialModelProducesCorrectModel() {
    val model = Model(
        loadingDriverStandings = true,
        driverStandings = emptyList(),
        driverStandingsError = Throwable()
    )
    val changes = PartialModel.DriverStandingsLoaded(list)
    assertThat(reducer(model, changes)).isEqualTo(model.copy(
        loadingDriverStandings = false,
        driverStandings = changes.items,
        driverStandingsError = null
    ))
  }
}

private inline val list: List<DriverStandingItem> get() = listOf(
    DriverStandingItem(
        season = randomUUID(),
        round = randomUUID(),
        position = randomUUID(),
        positionText = randomUUID(),
        points = randomUUID(),
        wins = randomUUID(),
        driverItem = DriverItem(
            randomUUID(),
            randomUUID(),
            randomUUID(),
            randomUUID(),
            randomUUID(),
            randomUUID(),
            LocalDate.now(),
            randomUUID()
        ),
        constructorItems = listOf(ConstructorItem(
            randomUUID(),
            randomUUID(),
            randomUUID(),
            randomUUID()
        ))
    )
)
