package io.github.drymarev.neptune.schedule

import com.google.common.truth.Truth.assertThat
import io.github.drymarev.neptune.core.Reducer
import org.junit.Before
import org.junit.Test
import org.threeten.bp.OffsetDateTime
import java.util.UUID
import kotlin.properties.Delegates

class ScheduleModulReducerTest {

  private var reducer by Delegates.notNull<Reducer<ScheduleModel, SchedulePartialModel>>()

  @Before fun setUp() {
    reducer = ScheduleModelReducer
  }

  @Test fun loadingSchedulePartialModelProducesCorrectModel() {
    val model = ScheduleModel(
        loadingSchedule = false,
        schedule = list,
        scheduleError = Throwable()
    )
    val changes = SchedulePartialModel.LoadingSchedule
    assertThat(reducer(model, changes)).isEqualTo(model.copy(
        loadingSchedule = true,
        schedule = emptyList(),
        scheduleError = null
    ))
  }

  @Test fun scheduleLoadedPartialModelProducesCorrectModel() {
    val model = ScheduleModel(
        loadingSchedule = true,
        schedule = emptyList(),
        scheduleError = Throwable()
    )
    val changes = SchedulePartialModel.ScheduleLoaded(list)
    assertThat(reducer(model, changes)).isEqualTo(model.copy(
        loadingSchedule = false,
        schedule = changes.items,
        scheduleError = null
    ))
  }

  @Test fun scheduleErrorPartialModelProducesCorrectModel() {
    val model = ScheduleModel(
        loadingSchedule = true,
        schedule = list,
        scheduleError = null
    )
    val changes = SchedulePartialModel.ScheduleError(Throwable())
    assertThat(reducer(model, changes)).isEqualTo(model.copy(
        loadingSchedule = false,
        schedule = emptyList(),
        scheduleError = changes.error
    ))
  }

  private inline val list
    get() = listOf(ScheduleItem(
        "${UUID.randomUUID()}",
        "${UUID.randomUUID()}",
        OffsetDateTime.now(),
        "${UUID.randomUUID()}",
        "${UUID.randomUUID()}",
        "${UUID.randomUUID()}",
        "${UUID.randomUUID()}",
        "${UUID.randomUUID()}",
        "${UUID.randomUUID()}",
        "${UUID.randomUUID()}",
        "${UUID.randomUUID()}"
    ))
}
