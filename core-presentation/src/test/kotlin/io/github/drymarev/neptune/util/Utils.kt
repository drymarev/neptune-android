package io.github.drymarev.neptune.util

import java.util.UUID

fun randomUUID(): String = "${UUID.randomUUID()}"
