package io.github.drymarev.neptune;

import java.io.IOException;
import okhttp3.ResponseBody;
import retrofit2.Converter;

final class MRDataConverter<T> implements Converter<ResponseBody, T> {

  private final Converter<ResponseBody, MRData<T>> delegate;

  MRDataConverter(Converter<ResponseBody, MRData<T>> delegate) {
    this.delegate = delegate;
  }

  @Override public T convert(ResponseBody value) throws IOException {
    MRData<T> mrData = delegate.convert(value);
    return mrData.getData();
  }
}
