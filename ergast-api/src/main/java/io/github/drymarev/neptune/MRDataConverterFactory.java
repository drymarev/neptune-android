package io.github.drymarev.neptune;

import com.squareup.moshi.Types;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

public final class MRDataConverterFactory extends Converter.Factory {

  public static Converter.Factory create() {
    return new MRDataConverterFactory();
  }

  private MRDataConverterFactory() {
  }

  @Override
  public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations,
      Retrofit retrofit) {
    Type mrDataType = Types.newParameterizedType(MRData.class, type);
    Converter<ResponseBody, Converter> delegate =
        retrofit.nextResponseBodyConverter(this, mrDataType, annotations);
    //noinspection unchecked
    return new MRDataConverter(delegate);
  }
}
