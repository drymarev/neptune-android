package io.github.drymarev.neptune

import com.squareup.moshi.FromJson
import com.squareup.moshi.Json
import com.squareup.moshi.ToJson
import io.reactivex.Single
import org.threeten.bp.LocalDate
import org.threeten.bp.OffsetDateTime
import org.threeten.bp.OffsetTime
import org.threeten.bp.ZoneOffset
import org.threeten.bp.format.DateTimeFormatter
import retrofit2.http.GET
import retrofit2.http.Path

interface ErgastService {

  @GET("api/f1/{season}.json")
  fun schedule(@Path("season") season: String): Single<ScheduleResponse>

  @GET("api/f1/{season}/driverstandings.json")
  fun driverStandings(@Path("season") season: String): Single<DriverStandingsResponse>

  @GET("api/f1/{season}/constructorstandings.json")
  fun constructorStandings(@Path("season") season: String): Single<ConstructorStandingsResponse>
}

data class MRData<out T>(@Json(name = "MRData") val data: T)

data class ScheduleResponse(
    val xmlns: String,
    val series: String,
    val url: String,
    val limit: Int,
    val offset: Int,
    val total: Int,
    @Json(name = "RaceTable") val raceTable: RaceTable
)

data class RaceTable(val season: String, @Json(name = "Races") val races: List<Race>)

data class Race(
    val season: String,
    val round: String,
    val url: String,
    val raceName: String,
    val circuit: Circuit,
    val dateTime: OffsetDateTime
)

data class RaceJson(
    val season: String,
    val round: String,
    val url: String,
    val raceName: String,
    @Json(name = "Circuit") val circuit: Circuit,
    val date: String,
    val time: String? = null
)

class RaceJsonAdapter {

  @FromJson fun raceFromJson(json: RaceJson): Race {
    return Race(
        season = json.season,
        round = json.round,
        url = json.url,
        raceName = json.raceName,
        circuit = json.circuit,
        dateTime = OffsetDateTime.parse(when (json.time) {
          null -> "${json.date}T00:00:00Z"
          else -> "${json.date}T${json.time}"
        }, DateTimeFormatter.ISO_OFFSET_DATE_TIME)
    )
  }

  @ToJson fun raceToJson(race: Race): RaceJson {
    return RaceJson(
        season = race.season,
        round = race.round,
        url = race.url,
        raceName = race.raceName,
        circuit = race.circuit,
        date = DateTimeFormatter.ISO_OFFSET_DATE.format(race.dateTime),
        time = when {
          race.dateTime.toOffsetTime().isEqual(OffsetTime.of(0, 0, 0, 0, ZoneOffset.UTC)) -> null
          else -> DateTimeFormatter.ISO_OFFSET_TIME.format(race.dateTime)
        }
    )
  }
}

data class Circuit(
    @Json(name = "circuitId") val id: String,
    val url: String,
    @Json(name = "circuitName") val name: String,
    @Json(name = "Location") val location: Location
)

data class Location(
    val lat: String,
    @Json(name = "long") val lon: String,
    val locality: String,
    val country: String
)

data class DriverStandingsResponse(
    val xmlns: String,
    val series: String,
    val url: String,
    val limit: Int,
    val offset: Int,
    val total: Int,
    @Json(name = "StandingsTable") val table: DriverStandingsTable
)

data class DriverStandingsTable(
    val season: String,
    @Json(name = "StandingsLists") val lists: List<DriverStandingsList>
)

data class DriverStandingsList(
    val season: String,
    val round: String,
    @Json(name = "DriverStandings") val standings: List<DriverStanding>
)

data class DriverStanding(
    val position: String,
    val positionText: String,
    val points: String,
    val wins: String,
    @Json(name = "Driver") val driver: Driver,
    @Json(name = "Constructors") val constructors: List<Constructor>
)

data class Driver(
    @Json(name = "driverId") val id: String,
    val permanentNumber: String,
    val code: String,
    val url: String,
    val givenName: String,
    val familyName: String,
    val dateOfBirth: LocalDate,
    val nationality: String
)

data class Constructor(
    @Json(name = "constructorId") val id: String,
    val url: String,
    val name: String,
    val nationality: String
)

class LocalDateAdapter {

  @FromJson fun fromJson(json: String): LocalDate {
    return LocalDate.parse(json)
  }

  @ToJson fun toJson(date: LocalDate): String {
    return date.toString()
  }
}

data class ConstructorStandingsResponse(
    val xmlns: String,
    val series: String,
    val url: String,
    val limit: Int,
    val offset: Int,
    val total: Int,
    @Json(name = "StandingsTable") val table: ConstructorStandingsTable
)

data class ConstructorStandingsTable(
    val season: String,
    @Json(name = "StandingsLists") val lists: List<ConstructorStandingsList>
)

data class ConstructorStandingsList(
    val season: String,
    val round: String,
    @Json(name = "ConstructorStandings") val standings: List<ConstructorStanding>
)

data class ConstructorStanding(
    val position: String,
    val positionText: String,
    val points: String,
    val wins: String,
    @Json(name = "Constructor") val constructor: Constructor
)
