package io.github.drymarev.neptune.constructor_standings

import io.github.drymarev.neptune.Constructor
import io.github.drymarev.neptune.ConstructorStandingsResponse
import io.github.drymarev.neptune.ErgastService
import io.github.drymarev.neptune.core.Result
import io.github.drymarev.neptune.core.UseCase
import io.reactivex.Observable
import io.reactivex.ObservableSource
import javax.inject.Inject
import io.github.drymarev.neptune.constructor_standings.ConstructorStandingRecords as Records
import io.github.drymarev.neptune.constructor_standings.LoadConstructorStandingsAction as Action

class RemoteLoadConstructorStandingsUseCase @Inject constructor(
    val service: ErgastService
) : UseCase<Action, Result<Records>> {

  override fun apply(upstream: Observable<Action>): ObservableSource<Result<Records>> {
    return upstream
        .flatMapSingle {
          service.constructorStandings(when (it) {
            Action.CurrentSeason -> "current"
            is Action.Season -> it.season
          })
        }
        .map<Result<Records>> { Result.Success(it.toConstructorStandings()) }
        .onErrorReturn { Result.Failure(it) }
  }
}

private fun ConstructorStandingsResponse.toConstructorStandings(): Records {
  return table.lists.flatMap { (season, round, standings) ->
    standings.map { (position, positionText, points, wins, constructor) ->
      ConstructorStandingRecord(
          season = season,
          round = round,
          position = position,
          positionText = positionText,
          points = points,
          wins = wins,
          constructorRecord = constructor.constructorRecord
      )
    }
  }
}

internal val Constructor.constructorRecord: ConstructorRecord
  inline get() {
    return ConstructorRecord(
        id = id,
        url = url,
        name = name,
        nationality = nationality
    )
  }
