package io.github.drymarev.neptune.driver_standings

import io.github.drymarev.neptune.Constructor
import io.github.drymarev.neptune.Driver
import io.github.drymarev.neptune.DriverStandingsResponse
import io.github.drymarev.neptune.ErgastService
import io.github.drymarev.neptune.constructor_standings.ConstructorRecord
import io.github.drymarev.neptune.constructor_standings.constructorRecord
import io.github.drymarev.neptune.core.Result
import io.github.drymarev.neptune.core.UseCase
import io.reactivex.Observable
import io.reactivex.ObservableSource
import javax.inject.Inject
import io.github.drymarev.neptune.driver_standings.DriverStandingRecords as Records
import io.github.drymarev.neptune.driver_standings.LoadDriverStandingsAction as Action

class RemoteLoadDriverStandingsUseCase @Inject constructor(
    val service: ErgastService
) : UseCase<Action, Result<Records>> {

  override fun apply(upstream: Observable<Action>): ObservableSource<Result<Records>> {
    return upstream
        .flatMapSingle {
          service.driverStandings(when (it) {
            Action.CurrentSeason -> "current"
            is Action.Season -> it.season
          })
        }
        .map<Result<Records>> { Result.Success(it.toDriverStandingRecords()) }
        .onErrorReturn { Result.Failure(it) }
  }
}

private fun DriverStandingsResponse.toDriverStandingRecords(): List<DriverStandingRecord> {
  return table.lists.flatMap { (season, round, standings) ->
    standings.map { (position, positionText, points, wins, driver, constructors) ->
      DriverStandingRecord(
          season = season,
          round = round,
          position = position,
          positionText = positionText,
          points = points,
          wins = wins,
          driverRecord = driver.driverRecord,
          constructorRecords = constructors.toConstructorRecords()
      )
    }
  }
}

private val Driver.driverRecord: DriverRecord
  inline get() {
    return DriverRecord(
        id = id,
        permanentNumber = permanentNumber,
        code = code,
        url = url,
        givenName = givenName,
        familyName = familyName,
        dateOfBirth = dateOfBirth,
        nationality = nationality
    )
  }

private fun List<Constructor>.toConstructorRecords(): List<ConstructorRecord> {
  return map { it.constructorRecord }
}
