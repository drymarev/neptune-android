package io.github.drymarev.neptune.schedule

import io.github.drymarev.neptune.ErgastService
import io.github.drymarev.neptune.ScheduleResponse
import io.github.drymarev.neptune.core.Result
import io.github.drymarev.neptune.core.UseCase
import io.reactivex.Observable
import io.reactivex.ObservableSource
import javax.inject.Inject
import io.github.drymarev.neptune.schedule.LoadScheduleAction as Action
import io.github.drymarev.neptune.schedule.ScheduleRecords as Records

class RemoteLoadScheduleUseCase @Inject constructor(
    val service: ErgastService
) : UseCase<Action, Result<Records>> {

  override fun apply(upstream: Observable<Action>): ObservableSource<Result<Records>> {
    return upstream
        .flatMapSingle {
          service.schedule(when (it) {
            Action.CurrentSeason -> "current"
            is Action.Season -> it.season
          })
        }
        .map<Result<Records>> { Result.Success(it.toScheduleRecords()) }
        .onErrorReturn { Result.Failure(it) }
  }
}

private fun ScheduleResponse.toScheduleRecords(): List<ScheduleRecord> {
  return raceTable.races.map {
    ScheduleRecord(
        season = it.season,
        round = it.round,
        time = it.dateTime,
        raceName = it.raceName,
        raceUrl = it.url,
        circuitId = it.circuit.id,
        circuitName = it.circuit.name,
        circuitUrl = it.circuit.url,
        circuitCountry = it.circuit.location.country,
        circuitLocality = it.circuit.location.locality,
        circuitLat = it.circuit.location.lat,
        circuitLon = it.circuit.location.lon
    )
  }
}
