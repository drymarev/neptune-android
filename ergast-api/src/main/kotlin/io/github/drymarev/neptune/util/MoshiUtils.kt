package io.github.drymarev.neptune.util

import com.squareup.moshi.Moshi

fun buildMoshi(): Moshi {
  return Moshi.Builder().build()
}

inline fun buildMoshi(actions: Moshi.Builder.() -> Unit): Moshi {
  return Moshi.Builder().apply(actions).build()
}
