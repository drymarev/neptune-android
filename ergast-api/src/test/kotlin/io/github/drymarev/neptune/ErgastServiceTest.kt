package io.github.drymarev.neptune

import com.google.common.truth.Truth.assertThat
import io.github.drymarev.neptune.util.assertRequestCount
import io.github.drymarev.neptune.util.buildMoshi
import io.github.drymarev.neptune.util.buildRetrofitService
import io.github.drymarev.neptune.util.mockResponse
import io.github.drymarev.neptune.util.mockWebServer
import io.github.drymarev.neptune.util.setBody
import io.github.drymarev.neptune.util.takeRequest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.threeten.bp.LocalDate
import org.threeten.bp.OffsetDateTime
import org.threeten.bp.ZoneOffset
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import kotlin.properties.Delegates

class ErgastServiceTest {

  @get:Rule val server = mockWebServer()

  private val baseUrl = "/"
  private val moshi by lazy {
    buildMoshi {
      add(RaceJsonAdapter())
      add(LocalDateAdapter())
    }
  }

  private var service by Delegates.notNull<ErgastService>()

  @Before fun setUp() {
    service = buildRetrofitService {
      baseUrl(server.url(baseUrl))
      addConverterFactory(MRDataConverterFactory.create())
      addConverterFactory(MoshiConverterFactory.create(moshi))
      addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    }
  }

  @Test fun scheduleIsCorrectlyDeserialized() {
    val stream = javaClass.getResourceAsStream("schedule.json")
    server.enqueue(mockResponse { setBody(stream) })

    val expectedResponse = ScheduleResponse(
        xmlns = "http://ergast.com/mrd/1.4",
        series = "f1",
        url = "http://ergast.com/api/f1/current.json",
        limit = 30,
        offset = 0,
        total = 1,
        raceTable = RaceTable(
            season = "2017",
            races = listOf(Race(
                season = "2017",
                round = "1",
                url = "https://en.wikipedia.org/wiki/2017_Australian_Grand_Prix",
                raceName = "Australian Grand Prix",
                circuit = Circuit(
                    id = "albert_park",
                    url = "http://en.wikipedia.org/wiki/Melbourne_Grand_Prix_Circuit",
                    name = "Albert Park Grand Prix Circuit",
                    location = Location(
                        lat = "-37.8497",
                        lon = "144.968",
                        locality = "Melbourne",
                        country = "Australia"
                    )
                ),
                dateTime = OffsetDateTime.of(2017, 3, 26, 5, 0, 0, 0, ZoneOffset.UTC)
            ))
        )
    )
    with(service.schedule("current").test()) {
      assertNoErrors()
      assertValueCount(1)
      assertValue(expectedResponse)
    }

    server.assertRequestCount(1)
    server.takeRequest {
      assertThat(path).isEqualTo("${baseUrl}api/f1/current.json")
      assertThat(method).isEqualTo("GET")
    }
  }

  @Test fun driverStandingsAreCorrectlyDeserialized() {
    val stream = javaClass.getResourceAsStream("driver-standings.json")
    server.enqueue(mockResponse { setBody(stream) })

    val expectedResponse = DriverStandingsResponse(
        xmlns = "http://ergast.com/mrd/1.4",
        series = "f1",
        url = "http://ergast.com/api/f1/current/driverstandings.json",
        limit = 30,
        offset = 0,
        total = 1,
        table = DriverStandingsTable(
            season = "2017",
            lists = listOf(DriverStandingsList(
                season = "2017",
                round = "5",
                standings = listOf(DriverStanding(
                    position = "1",
                    positionText = "1",
                    points = "104",
                    wins = "2",
                    driver = Driver(
                        id = "vettel",
                        permanentNumber = "5",
                        code = "VET",
                        url = "http://en.wikipedia.org/wiki/Sebastian_Vettel",
                        givenName = "Sebastian",
                        familyName = "Vettel",
                        dateOfBirth = LocalDate.of(1987, 7, 3),
                        nationality = "German"
                    ),
                    constructors = listOf(Constructor(
                        id = "ferrari",
                        url = "http://en.wikipedia.org/wiki/Scuderia_Ferrari",
                        name = "Ferrari",
                        nationality = "Italian"
                    ))
                ))
            ))
        )
    )

    with(service.driverStandings("current").test()) {
      assertNoErrors()
      assertValueCount(1)
      assertValue(expectedResponse)
    }

    server.assertRequestCount(1)
    server.takeRequest {
      assertThat(path).isEqualTo("${baseUrl}api/f1/current/driverstandings.json")
      assertThat(method).isEqualTo("GET")
    }
  }

  @Test fun constructorStandingsAreCorrectlyDeserialized() {
    val stream = javaClass.getResourceAsStream("constructor-standings.json")
    server.enqueue(mockResponse { setBody(stream) })

    val expectedResponse = ConstructorStandingsResponse(
        xmlns = "http://ergast.com/mrd/1.4",
        series = "f1",
        url = "http://ergast.com/api/f1/current/constructorstandings.json",
        limit = 30,
        offset = 0,
        total = 1,
        table = ConstructorStandingsTable(
            season = "2017",
            lists = listOf(ConstructorStandingsList(
                season = "2017",
                round = "5",
                standings = listOf(ConstructorStanding(
                    position = "1",
                    positionText = "1",
                    points = "161",
                    wins = "3",
                    constructor = Constructor(
                        id = "mercedes",
                        url = "http://en.wikipedia.org/wiki/Mercedes-Benz_in_Formula_One",
                        name = "Mercedes",
                        nationality = "German"
                    )
                ))
            ))
        )
    )

    with(service.constructorStandings("current").test()) {
      assertNoErrors()
      assertValueCount(1)
      assertValue(expectedResponse)
    }

    server.assertRequestCount(1)
    server.takeRequest {
      assertThat(path).isEqualTo("${baseUrl}api/f1/current/constructorstandings.json")
      assertThat(method).isEqualTo("GET")
    }
  }
}
