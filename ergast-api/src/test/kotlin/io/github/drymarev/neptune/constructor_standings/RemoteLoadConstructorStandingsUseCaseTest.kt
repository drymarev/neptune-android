package io.github.drymarev.neptune.constructor_standings

import com.google.common.truth.Truth.assertThat
import io.github.drymarev.neptune.ErgastService
import io.github.drymarev.neptune.MRDataConverterFactory
import io.github.drymarev.neptune.core.Result
import io.github.drymarev.neptune.core.UseCase
import io.github.drymarev.neptune.util.assertRequestCount
import io.github.drymarev.neptune.util.buildMoshi
import io.github.drymarev.neptune.util.buildRetrofitService
import io.github.drymarev.neptune.util.mockResponse
import io.github.drymarev.neptune.util.mockWebServer
import io.github.drymarev.neptune.util.setBody
import io.github.drymarev.neptune.util.takeRequest
import io.reactivex.Observable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.HttpException
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import kotlin.properties.Delegates
import io.github.drymarev.neptune.constructor_standings.LoadConstructorStandingsAction as Action

class RemoteLoadConstructorStandingsUseCaseTest {

  @get:Rule val server = mockWebServer()

  private val baseUrl = "/"
  private val moshi by lazy { buildMoshi() }

  private var service by Delegates.notNull<ErgastService>()
  private var useCase by Delegates.notNull<UseCase<Action, Result<List<ConstructorStandingRecord>>>>()

  @Before fun setUp() {
    service = buildRetrofitService {
      baseUrl(server.url(baseUrl))
      addConverterFactory(MRDataConverterFactory.create())
      addConverterFactory(MoshiConverterFactory.create(moshi))
      addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    }
    useCase = RemoteLoadConstructorStandingsUseCase(service)
  }

  @Test fun loadConstructorStandingsResultFailureIsReturnedIfErrorIsEncountered() {
    val code = 404
    server.enqueue(mockResponse { setResponseCode(code) })

    val action = Action.CurrentSeason

    with(Observable.just(action).compose(useCase).test()) {
      assertNoErrors()
      assertValueCount(1)
      assertThat(values().first()).isInstanceOf(Result.Failure::class.java)
      val value = values().first() as Result.Failure
      assertThat(value.error).isInstanceOf(HttpException::class.java)
      assertThat((value.error as HttpException).code()).isEqualTo(code)
    }

    server.assertRequestCount(1)
    server.takeRequest {
      assertThat(path).isEqualTo("${baseUrl}api/f1/current/constructorstandings.json")
      assertThat(method).isEqualTo("GET")
    }
  }

  @Test fun loadDriverStandingsResultSuccessIsReturnedOnSuccessfulResponseWhenActionIsCurrentSeason() {
    val body = javaClass.getResourceAsStream("constructor-standings.json")
    server.enqueue(mockResponse { setBody(body) })

    val action = Action.CurrentSeason

    with(Observable.just(action).compose(useCase).test()) {
      assertNoErrors()
      assertValueCount(1)
      assertThat(values().first()).isInstanceOf(Result.Success::class.java)
      val value = values().first() as Result.Success
      assertThat(value.value).isNotEmpty()
      assertThat(value.value).hasSize(10)
    }

    server.assertRequestCount(1)
    server.takeRequest {
      assertThat(path).isEqualTo("${baseUrl}api/f1/current/constructorstandings.json")
      assertThat(method).isEqualTo("GET")
    }
  }

  @Test fun loadDriverStandingsResultSuccessIsReturnedOnSuccessfulResponseWhenActionIsSeason() {
    val body = javaClass.getResourceAsStream("constructor-standings.json")
    server.enqueue(mockResponse { setBody(body) })

    val action = Action.Season("2017")

    with(Observable.just(action).compose(useCase).test()) {
      assertNoErrors()
      assertValueCount(1)
      assertThat(values().first()).isInstanceOf(Result.Success::class.java)
      val value = values().first() as Result.Success
      assertThat(value.value).isNotEmpty()
      assertThat(value.value).hasSize(10)
    }

    server.assertRequestCount(1)
    server.takeRequest {
      assertThat(path).isEqualTo("${baseUrl}api/f1/${action.season}/constructorstandings.json")
      assertThat(method).isEqualTo("GET")
    }
  }
}
