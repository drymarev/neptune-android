package io.github.drymarev.neptune.util

import okio.Okio
import java.io.InputStream

fun InputStream.readUtf8(): String = Okio.buffer(Okio.source(this)).readUtf8()
